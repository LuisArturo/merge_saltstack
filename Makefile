clean:
	@rm -f out.yml *.pyc
	@find . -type d -name __pycache__ | xargs rm -fr
	@echo out.yml removed

test: test1 test2 test_python
# test: test1 test2 test3 test4 test_python

test1:
	@python3 merger.py files/first.yml files/second.yml -t base > out.yml
	@diff out.yml files/out_target.yml
	@echo test1 OK

test2:
	@python3 merger.py files/first2.yml files/second2.yml -t infra > out.yml
	@diff out.yml files/out_target2.yml
	@echo test2 OK

test3:
	@python3 merger.py data/first3.yml data/second3.yml data/third3.yml -t base > out.yml
	@diff out.yml data/out_target3.yml
	@echo test3 OK

test4:
	@python3 merger.py data/first4.yml data/second4.yml data/third4.yml data/fourth4.yml -t base > out.yml
	@diff out.yml data/out_target4.yml
	@echo test4 OK

test_python:
	@python3 tests.py
	@echo test_python OK

install:
	@pip3 install ruamel.yaml
	@pip3 install flake8

check_code:
	@flake8 merger.py
