- Nous avons besoin de faire un merge avec 3 et même 4 environnements/fichiers
  et ce n'est pour l'instant pas possible avec la version actuelle, le but
  serait donc de pouvoir faire cette manipulation pour reprendre l'existant:

first3.yml + second3.yml + third3.yml > out_target3.yml

first4.yml + second4.yml + third4.yml + fourth4.yml > out_target4.yml

J'ai mis en PJ les fichiers afin de les inclure dans le repo, j'ai
aussi mis à jour le makefile pour nous.


first3.yml
second3.yml
third3.yml

out_target3.yml

first4.yml
second4.yml
third4.yml
fourth4.yml

out_target4.yml

Makefile_ubiquity
README.rst

