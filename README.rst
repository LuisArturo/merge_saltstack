Install dependencies::

  $ git clone https://gitlab.zettafox.com/foss/merge_saltstack.git
  $ cd merge_saltstack
  $ pip install ruamel.yaml

Test::

  $ make test
  test1 OK
  test2 OK
  .
  ----------------------------------------------------------------------
  Ran 1 test in 0.013s
  OK
  test3 OK
  $

To get help::

  $ python3 merger.py -h
  ...
  $

Run::

  $ python3 merger.py -t base test/first.yml test/second.yml > out.yml
  $ diff out.yml test/out_target.yml
  $

Test with real salt yaml::

  $ make test2  # use data from orignal salt
  $ diff out.yml out_target2.yml
  $
