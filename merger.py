"""
More than 2 files (Not implemented)::

  $ python merger.py base f1.yml f2.yml f3.yml
    $ python merger.py base f1.yml f2.yml -> out1.yml
    $ python merger.py base out1.yml f3.yml -> out_final.yml
  $ python merger.py base /abs/path/to/f1.yml /some/other/path/f2.yml - stdout

Specification
==============
1. cat first.yml
2. start cat second.yml
  2.1 if second key new add it
  2.2 if second.newkey exist in first.keys move existing from
         first.key to new second.newkey position
  2.3 updating newkey with values in first.key
3. si first.key33.match != second.key33.match raise error
     - match:typeA
     - DONE in tests.py
4. if state in first.key.states
     - Done
     - state should not be in sec.key.states
"""
# from os.path import basename
# from os.path import splitext
# from pdb import set_trace
from collections import OrderedDict, defaultdict
from os.path import abspath
from sys import stdout
import argparse

from ruamel.yaml import YAML


class Logger:
    "utf-8 stdout"
    def write(self, msg):
        stdout.write(msg.decode('utf-8'))


STDOUT = Logger()


def load_yaml_files(fn1, fn2):
    "Load files"
    firstfile, secondfile = abspath(fn1), abspath(fn2)
    with open(firstfile, 'r') as yfile:
        yml1 = YAML().load(yfile)
    with open(secondfile, 'r') as yfile:
        yml2 = YAML().load(yfile)
    yml1.filename = fn1
    yml2.filename = fn2
    return yml1, yml2


def remove_duplicate(vals1, vals2):
    """
    Return vals2 without duplicates from vals1

    vals1 = first.get('*-dev or *-int or *-preprod or *-prod')
    # [ordereddict([('match', 'compound')]), 'daemon',
       'monitoring.rsyslog.prod']
    vals2 = second.get('*-dev or *-int or *-preprod or *-prod')
    # [ordereddict([('match', 'compound')]), 'sarje.home', 'jvm']
    out = remove_duplicate(vals1, vals2)
    # 'sarje.home', 'jvm']
    """
    out = []
    for val in vals2:
        if val not in vals1:
            out.append(val)
    return out


def stdout_final(newyaml, newyml):
    "Indent and print"
    complete = {}
    complete[newyaml] = dict(newyml)
    # from collections import OrderedDict
    # complete[newyaml] = OrderedDict(newyml)
    yaml = YAML()
    yaml.indent(mapping=4, sequence=6, offset=3)
    yaml.dump(complete, STDOUT)


def inspect(yml_dict):
    yaml = YAML()
    yaml.indent(mapping=4, sequence=6, offset=3)
    print('=====> ')
    print(yaml.dump(yml_dict, STDOUT))
    print('=====> ')


def remove_top(yml1, yml2):
    "Do not take the very first yaml level e.g. take level"
    first = OrderedDict(list(yml1.values())[0])
    second = OrderedDict(list(yml2.values())[0])
    first.filename = yml1.filename
    second.filename = yml2.filename
    return first, second


def get_matchs(target):
    """
    Return a dict with key: match
    >>> yml1, yml2 = load_yaml_files('files/first2.yml', 'files/second2.yml')
    >>> first, second = remove_top(yml1, yml2)
    >>> print(get_matchs(second)
    {'*-dev or *-int or *-preprod or *-prod': [('match', 'compound')],
     'not sarje-infra': [('match', 'compound')]}
    >>>
    """
    res = {}
    for key, vals in target.items():
        for val in vals:
            if isinstance(val, OrderedDict):
                match = list(val.items())
                if 'match' in match[0][0]:
                    res[key] = match
    return res


def get_states(target):
    """
    Return a dict with key: match
    >>> yml1, yml2 = load_yaml_files('files/first2.yml', 'files/second2.yml')
    >>> first, second = remove_top(yml1, yml2)
    >>> print(get_states(second)['*'])
    ['ntp', 'hostname']
    >>>
    """
    res = {}
    for key, states in target.items():
        our_states = []
        for state in states:
            if not isinstance(state, OrderedDict):
                our_states.append(state)
        res[key] = our_states
    return res


def check_states(first, second):
    """ """
    states2 = get_states(second)
    msg_ex = "target::f1:f2\n                {}::{}:{}"
    msg_ex += "\n   both have this state(s): {}"
    for key1, value1 in get_states(first).items():
        if key1 in states2.keys():
            value2 = states2[key1]
            duplicate = set(value1).intersection(set(value2))
            if duplicate:
                raise Exception(msg_ex.format(key1, first.filename,
                                second.filename, duplicate))


def check_matchs(first, second):
    """
    Raise exeption if same keys has different match value
    >>> bad = 'files/second2_bad_match.yml'
    >>> yml1, yml2 = load_yaml_files('files/second2.yml', bad)
    >>> first, second = remove_top(yml1, yml2)
    >>> check_matchs(first, second)
    Exception: key: *-dev or *-int or *-preprod or *-prod:
      match1: [('match', 'compound')]
      match2: [('match', 'cccompound')]
    >>>
    """
    matches2 = get_matchs(second)
    msg = "key: {}: \n  match1: {}\n  match2: {}"
    for key1, value1 in get_matchs(first).items():
        if key1 in matches2.keys():
            value2 = matches2[key1]
            if value1 != value2:
                raise Exception(msg.format(key1, value1, value2))


def args_from_parser():
    "Argparse"
    usage = '%(prog)s [-t top] /path/to/first.yml .../second.yml'
    description = 'Merge Salt Stack, Ansible files'
    parser = argparse.ArgumentParser(description=description, usage=usage)
    parser.add_argument("-t", "--top", type=str, help="yaml top name")
    parser.add_argument('yamls', nargs='+', help="yamls files")
    args = parser.parse_args()
    return args.top, args.yamls   # firstyaml, args.secondyaml


def two_first(top, first, second):

    start = defaultdict(list)
    for key, value in first.items():
        if key not in second.keys():
            start[key].extend(value)
    newyml = defaultdict(list, start)
    for key, value in second.items():
        if key not in first.keys():
            newyml[key].extend(value)
        else:
            vals1 = first.get(key)
            newyml[key].extend(first.get(key))
            newvals = remove_duplicate(vals1, value)
            newyml[key].extend(newvals)
    return top, newyml


def main(top, yamls):
    "Do the job"
    firstyaml = yamls.pop(0)
    secondyaml = yamls.pop(0)
    yml1, yml2 = load_yaml_files(firstyaml, secondyaml)
    first, second = remove_top(yml1, yml2)

    top, newyml = two_first(top, first, second)

    if yamls:
        ynext = yamls.pop(0)
        ymlnext, ymlnext = load_yaml_files(ynext, ynext)
        ynext, ynext= remove_top(ymlnext, ymlnext)
        top, newyml = two_first(top, newyml, ynext)
    stdout_final(top, newyml)


if __name__ == '__main__':
    TOP, YAMLS = args_from_parser()
    main(TOP, YAMLS)
