import unittest
from merger import load_yaml_files
from merger import remove_top
from merger import check_matchs
from merger import check_states
from merger import get_states


class SaltTest(unittest.TestCase):
    def test_check_matchs(self):
        """
        >>> check_matchs(first, second)
        Exception: key: *-dev or *-int or *-preprod or *-prod:
          match1: [('match', 'compound')]
          match2: [('match', 'cccompound')]
        >>>
        """
        bad = 'files/second2_bad_match.yml'
        yml1, yml2 = load_yaml_files('files/second2.yml', bad)
        first, second = remove_top(yml1, yml2)
        with self.assertRaises(Exception):
            check_matchs(first, second)

    def test_check_states(self):
        bad = 'files/first2.yml'
        yml1, yml2 = load_yaml_files('files/first2.yml', bad)
        first, second = remove_top(yml1, yml2)
        with self.assertRaises(Exception):
            check_states(first, second)

        bad = 'files/second2_bad_state.yml'
        yml1, yml2 = load_yaml_files('files/first2.yml', bad)
        first, second = remove_top(yml1, yml2)
        with self.assertRaises(Exception):
            check_states(first, second)

        bad = 'files/second2.yml'
        yml1, yml2 = load_yaml_files('files/first2.yml', bad)
        first, second = remove_top(yml1, yml2)
        check_states(first, second)
        self.assertTrue(True)


    def test_get_states(self):
        yml1, yml2 = load_yaml_files('files/first2.yml', 'files/second2.yml')
        first, second = remove_top(yml1, yml2)
        self.assertListEqual(get_states(second)['*'], ['ntp', 'hostname'])



if __name__ == '__main__':
    unittest.main()
